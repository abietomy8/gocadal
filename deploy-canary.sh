# !/bin/bash
pwd
ls -al
BUILD_JOB_ID=$(cat build_job_id)
echo "Build Job ID: ${BUILD_JOB_ID}"
ARTIFACT_URL="${CI_PROJECT_URL}/-/jobs/${BUILD_JOB_ID}/artifacts/download?archive=zip"
echo "Artifact URL: ${ARTIFACT_URL}"
echo "START TO DEPLOY....."
echo "nomad address : ${NOMAD_STG_ADDRESS}"
echo "nomad token : ${NOMAD_STG_TOKEN} "
export NOMAD_ADDR=$NOMAD_STG_ADDRESS
export NOMAD_TOKEN=$NOMAD_STG_TOKEN 
export NOMAD_SKIP_VERIFY=true 
levant deploy byu-goapp-cms.nomad
    # -var git_sha="${CI_COMMIT_SHORT_SHA}" \ 
    # -var artifact_url="${ARTIFACT_URL}" \
    # cms-fake.nomad.hcl
# echo "SUCCESS DEPLOY!!"
