job "byu-goapp" {
  datacenters = ["tbs-1"]
  type = "service"
  namespace = "byu-default"
  
  group "byu-goapp" {
    network {
      port  "http"{
        to ="1012"
      }
    }
    
    task "byu-goapp" {
      env {
        PORT    = "1012"
      }
      driver = "docker"
      # artifact {
      #   source = "<no value>"
      # }
      config {
        image = "abietomy/web-cms"
        command = "cms-demo"
        ports = ["http"]
        auth {
          server_address = "abietomy/web-cms"
          username = "abietomy"
          password = "Spasikosong8*"
        }
      }
      resources {
        cpu = 1024
        memory = 2048
      }
      service {
        name = "byu-goapp"
        port = "http"
        check {
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
